import { useContext } from "react";
import filterContext from "./context";
export default function useFilters() {
  return useContext(filterContext);
}
