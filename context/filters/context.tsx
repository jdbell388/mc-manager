import { createContext } from "react";
const filterContext = createContext({
  all: { heros: [], aspects: [], villains: [], encounters: [] },
  active: { heros: [], aspects: [], villains: [], encounters: [] },
  actions: {},
});
export default filterContext;
