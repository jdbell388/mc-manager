import React, { useState, useReducer, useMemo } from "react";
import filterContext from "./context";
import { heros } from "../../api/heros";
import { aspects } from "../../api/aspects";
import { villains } from "../../api/villains";
import { encounters } from "../../api/encounters";
export default function ScenarioProvider({ children }: any) {
  const active = {
    heros: [...heros],
    aspects: [...aspects],
    villains: [...villains],
    encounters: [...encounters],
    players: 'one'
  }
  const INITIAL_STATE = {
    all: {
      heros,
      aspects,
      villains,
      encounters,
    },
    active
  };

  const formReducer = (state: any, action: any) => {
    switch (action.type) {
      case "RESET_FILTERS":
        return {
          ...state,
          active: {
            ...state.all,
          },
        };
      case "RESET_HEROS":
        return {
          ...state,
          active: {
            ...state.active, heros: state.all.heros
          },
        };
      case "CLEAR_HEROS":
        return {
          ...state,
          active: { ...state.active, heros: [] }
        }
      case "FILTER_HEROS":
        return {
          ...state,
          active: { ...state.active, heros: action.payload },
        };
      case "RESET_ASPECTS":
        return {
          ...state,
          active: {
            ...state.active, aspects: state.all.aspects
          },
        };
      case "CLEAR_ASPECTS":
        return {
          ...state,
          active: { ...state.active, aspects: [] }
        }
      case "FILTER_ASPECTS":
        return {
          ...state,
          active: { ...state.active, aspects: action.payload },
        };
      case "RESET_ENCOUNTERS":
        return {
          ...state,
          active: {
            ...state.active, encounters: state.all.encounters
          },
        };
      case "CLEAR_ENCOUNTERS":
        return {
          ...state,
          active: { ...state.active, encounters: [] }
        }
      case "FILTER_ENCOUNTERS":
        return {
          ...state,
          active: { ...state.active, encounters: action.payload },
        };
      case "RESET_VILLAINS":
        return {
          ...state,
          active: {
            ...state.active, villains: state.all.villains
          },
        };
      case "CLEAR_VILLAINS":
        return {
          ...state,
          active: { ...state.active, villains: [] }
        }
      case "FILTER_VILLAINS":
        return {
          ...state,
          active: { ...state.active, villains: action.payload },
        };
      case "SET_PLAYERS":
        return {
          ...state,
          active: { ...state.active, players: action.payload },
        };
      default:
        throw new Error();
    }
  };

  const [state, dispatch] = useReducer(formReducer, INITIAL_STATE);

  function filterArray(array: any, object: any, key: any) {
    var index = array.findIndex((o: any) => o[key] === object[key]);
    const newArray = array;
    if (index === -1) newArray.push(object);
    else newArray.splice(index, 1);
    return newArray;
  }

  const isActive = (val: any, type: any) => {
    if (state.active[type].some((hero: any) => hero.name === val.name)) {
      return true;
    } else {
      return false;
    }
  };

  const toggle = async (type: any, val: any) => {
    const array = state.active[type];
    const filtered = await filterArray(array, val, "name");
    switch (type) {
      case "heros":
        await dispatch({ type: "FILTER_HEROS", payload: filtered });
        return;
      case "aspects":
        await dispatch({ type: "FILTER_ASPECTS", payload: filtered });
        return;
      case "encounters":
        await dispatch({ type: "FILTER_ENCOUNTERS", payload: filtered });
        return;
      case "villains":
        await dispatch({ type: "FILTER_VILLAINS", payload: filtered });
        return;
      default:
        throw new Error();
    }
  };

  const setPlayers = async (players: string) => {
    dispatch({ type: 'SET_PLAYERS', payload: players });
  }

  const value = useMemo(() => ({
    state,
    dispatch,
    actions: { toggle, isActive, setPlayers },
  }), [state]);
  return <filterContext.Provider value={value}>{children}</filterContext.Provider>;
}
