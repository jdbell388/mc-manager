import React, { useMemo } from "react";
import { StyleSheet } from "react-native";
import { Button, Provider, Text, Headline } from "react-native-paper";
import FilterButtonWithModal from "../components/filters/FilterButton";
import useFilters from "../context/filters/filters";
import FilterHeros from '../components/filters/FilterHeros';
import FilterAspects from '../components/filters/FilterAspects';
import FilterEncounters from '../components/filters/FilterEncounters';
import FilterVillains from '../components/filters/FilterVillains';
import FilterPlayers from '../components/filters/FilterPlayers';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

import { View } from "../components/Themed";

const Stack = createNativeStackNavigator();

export default function TabTwoScreen() {
  const { state, dispatch, actions } = useFilters();
  const allAspects = state.all.aspects && useMemo(() => (state.all.aspects), [state.all.aspects]);
  return (
    <View style={styles.container}>
      <Headline style={styles.title}>Set Filters</Headline>
      <Provider>
        <View style={styles.filterContainer}>
          <FilterPlayers />
          <FilterHeros />
          <FilterAspects />
          <FilterEncounters />
          <FilterVillains />
        </View>
      </Provider>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  filterContainer: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    alignContent: 'center',
    justifyContent: 'space-between'
  },
  title: {
    fontWeight: "bold",
    marginTop: 20,
    marginBottom: 0
  },
  separator: {
    marginVertical: 30,
    height: 1,
    width: "80%",
  },
});
