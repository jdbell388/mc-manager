import * as React from "react";
import { StyleSheet } from "react-native";

import { Text, View } from "../components/Themed";
import GetRandom from "../components/get-random/GetRandom";
import ShowFilters from "../components/show-filters/ShowFilters";

export default function TabOneScreen() {
  return (
    <View style={styles.container}>
      <Text style={styles.title}>Generate a Scenario</Text>
      <GetRandom />
      {/* <ShowFilters /> */}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
  },
  title: {
    fontSize: 20,
    fontWeight: "bold",
    textAlign: 'center'
  },
  separator: {
    marginVertical: 30,
    height: 1,
    width: "80%",
  },
});
