export const heros = [
  {
    name: "Spider Man",
  },
  {
    name: "Captain Marvel",
  },
  {
    name: "She-Hulk",
  },
  {
    name: "Iron Man",
  },
  {
    name: "Black Panther",
  },
  {
    name: "Captain America",
  },
  {
    name: "Ms. Marvel",
  },
  {
    name: "Thor",
  },
  {
    name: "Black Widow",
  },
  {
    name: "Doctor Strange",
  },
  {
    name: "Hulk",
  },
  {
    name: "Hawkeye",
  },
  {
    name: "Spider-Woman",
  },
  {
    name: "Ant Man",
  },
  {
    name: "Wasp",
  },
  {
    name: "Quicksilver",
  },
  {
    name: "Scarlet Witch",
  },
  {
    name: "Rocket Raccoon",
  },
  {
    name: "Groot",
  },
  {
    name: "Star Lord",
  },
  {
    name: "Gamora",
  },
  {
    name: "Drax",
  },
  {
    name: "Venom",
  },
];
