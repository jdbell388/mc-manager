export const encounters = [
  {
    name: "Bomb Scare",
  },
  {
    name: "Masters of Evil",
  },
  {
    name: "Under Attack",
  },
  {
    name: "Legions of Hydra",
  },
  {
    name: "Doomsday Chair",
  },
  {
    name: "Goblin Gimmicks",
  },
  {
    name: "Mess of Things",
  },
  {
    name: "Power Drain",
  },
  {
    name: "Running Interference",
  },
  {
    name: "Kree Fanatic",
  },
  {
    name: "Experimental Weapons",
  },
  {
    name: "Hydra Assault",
  },
  {
    name: "Hydra Patrol",
  },
  {
    name: "Weapon Master",
  },
  {
    name: "Temporal",
  },
  {
    name: "Master of Time",
  },
  {
    name: "Anachronauts",
  },
  {
    name: "Band of Badoon",
  },
  {
    name: "Galactic Artifacts",
  },
  {
    name: "Kree Militants",
  },
  {
    name: "Menagerie Medley",
  },
  {
    name: "Space Pirates",
  },
  {
    name: "Ship Command",
  },
  {
    name: "Power Stone",
  },
];
