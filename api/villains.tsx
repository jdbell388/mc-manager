export const villains = [
  {
    name: "Rhino",
  },
  {
    name: "Klaw",
  },
  {
    name: "Ultron",
  },
  {
    name: "Norman Osborn / Green Goblin",
  },
  {
    name: "Green Goblin",
  },
  {
    name: "Crossbones",
  },
  {
    name: "Absorbing Man",
  },
  {
    name: "Taskmaster",
  },
  {
    name: "Zola",
  },
  {
    name: "Red Skull",
  },
  {
    name: "Kang",
  },
  {
    name: "Brotherhood of Badoon",
  },
  {
    name: "Infiltrate the Museum (Collector 1)",
  },
  {
    name: "Escape the Museum (Collector 2)",
  },
  {
    name: "Nebula",
  },
  {
    name: "Ronan",
  },
];
