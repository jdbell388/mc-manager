export const aspects = [
  {
    name: "Aggression",
  },
  {
    name: "Justice",
  },
  {
    name: "Leadership",
  },
  {
    name: "Protection",
  },
];
