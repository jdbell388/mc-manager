import React, { useState, useEffect } from "react";
import { View, Text } from "react-native";
import { Button, withTheme, List } from 'react-native-paper';
import { villains } from "../../api/villains";
import useFilters from "../../context/filters/filters";

const GetRandom = () => {
  const [playerOne, setPlayerOne] = useState<null | any>();
  const [playerTwo, setPlayerTwo] = useState<null | any>();
  const [playerThree, setPlayerThree] = useState<null | any>();
  const [playerFour, setPlayerFour] = useState<null | any>();
  const [aspect, setAspect] = useState<null | any>();
  const [villian, setVillian] = useState<null | any>();
  const [encounter, setEncounter] = useState<null | any>();
  const { state, actions } = useFilters();
  const numPlayers = state.active.players ? Number(state.active.players) : 1;
  const getRandomInt = (max: number) => {
    return Math.floor(Math.random() * Math.floor(max));
  };

  const getSets = (arr: any, players: any) => {
    const nums = new Set();
    while (nums.size !== players) {
      nums.add(getRandomInt(arr.length));
    }
    const result = [];
    for (let item of nums.values(0)) {
      result.push(arr[item]);
    }
    return result;
  }
  const randomVal = (arr: any, not?: any) => {
    const int = getRandomInt(arr.length);
    if (not === int) {
      randomVal(arr, not);
    } else {
      return arr[int];
    }
  };

  const generateScenario = () => {
    const aspectVal = randomVal(state.active.aspects);
    const villainVal = randomVal(state.active.villains);
    const encounterVal = randomVal(state.active.encounters);
    const playerVals = getSets(state.active.heros, numPlayers);
    setPlayerOne(playerVals[0]);
    if (playerVals[1]) {
      setPlayerTwo(playerVals[1]);
    }
    if (playerVals[2]) {
      setPlayerThree(playerVals[2]);
    }
    if (playerVals[3]) {
      setPlayerFour(playerVals[3]);
    }
    setAspect(aspectVal);
    setVillian(villainVal);
    setEncounter(encounterVal);
  }

  return (
    <View>
      <Button
        mode="contained"
        style={{ alignSelf: 'center', marginTop: 30, marginBottom: 15 }}
        contentStyle={{ paddingTop: 10, paddingBottom: 10, width: 300 }}
        labelStyle={{ fontSize: 20, fontWeight: 'bold' }}
        onPress={() => {
          generateScenario();
        }}
      >Generate Scenario</Button>
      { playerOne && (<List.Item
        title={playerOne.name}
        titleNumberOfLines={7}
        description="Player 1"
        left={props => <List.Icon {...props} icon="shield-account" />}
      />)}
      { playerTwo && (<List.Item
        title={playerTwo.name}
        titleNumberOfLines={7}
        description="Player 2"
        left={props => <List.Icon {...props} icon="shield-account" />}
      />)}
      { playerThree && (<List.Item
        title={playerThree.name}
        titleNumberOfLines={7}
        description="Player 3"
        left={props => <List.Icon {...props} icon="shield-account" />}
      />)}
      { playerFour && (<List.Item
        title={playerFour.name}
        titleNumberOfLines={7}
        description="Player 4"
        left={props => <List.Icon {...props} icon="shield-account" />}
      />)}
      { aspect && <List.Item
        title={aspect.name}
        titleNumberOfLines={2}
        description="Aspect"
        left={props => <List.Icon {...props} icon="progress-wrench" />}
      />}
      { villian && <List.Item
        title={villian.name}
        titleNumberOfLines={7}
        description="Villain"
        left={props => <List.Icon {...props} icon="emoticon-devil" />}
      />}
      { encounter && <List.Item
        title={encounter.name}
        titleNumberOfLines={4}
        description="Encounter Set"
        left={props => <List.Icon {...props} icon="target-account" />}
      />}




    </View >
  );
};

export default withTheme(GetRandom);
