import React from 'react'
import { Modal, Portal, Button, Provider, Text } from "react-native-paper";
import { View, StyleSheet } from "react-native";
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Filter from './Filter';

export default function FilterButtonWithModal(props: any) {
  const { checkedAction, checkedAllAction, objects } = props;
  const [visible, setVisible] = React.useState(false);

  const showModal = () => setVisible(true);
  const hideModal = () => setVisible(false);
  const containerStyle = { backgroundColor: "white", padding: 20 };
  return (
    <React.Fragment>
      <Portal>
        <Modal visible={visible} onDismiss={hideModal} contentContainerStyle={containerStyle}>
          <Filter checkedAction={checkedAction} checkedAllAction={checkedAllAction} objects={objects} />
        </Modal>
      </Portal>
      <Button style={styles.button} mode="contained" onPress={showModal}>
        <View style={styles.iconWrapper}>
          <Text style={styles.icon}><Icon name="progress-wrench" size={50} /></Text>
          <Text style={styles.label} >Filter Aspects</Text>
        </View>
      </Button>
    </React.Fragment>
  )
}

const styles = StyleSheet.create({
  button: {
    width: '50%',
    height: 150,
    display: 'flex',
    flexDirection: 'row',
    borderRadius: 0,
    alignContent: 'center',
    alignItems: 'center',
    justifyContent: 'center'
  },
  iconWrapper: {
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'center',
    alignContent: 'stretch',
    alignItems: 'center'
  },
  icon: {
    color: "white",
  },
  label: {
    width: '100%',
    color: 'white',
    textAlign: 'center',
    textTransform: 'uppercase',
    fontWeight: '500',
    marginTop: 15
  }
});