import React, { useMemo } from 'react'
import useFilters from "../../context/filters/filters";
import { Modal, Portal, Button, Provider, Text, Checkbox, RadioButton, List } from "react-native-paper";
import { ScrollView, View, StyleSheet } from "react-native";
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

export default function FilterPlayers() {
  const { state, actions } = useFilters();
  const [visible, setVisible] = React.useState(false);

  const showModal = () => setVisible(true);
  const hideModal = () => setVisible(false);
  const containerStyle = { backgroundColor: "white", padding: 20 };
  return (
    <React.Fragment>
      <Portal>
        <Modal visible={visible} onDismiss={hideModal} contentContainerStyle={containerStyle}>
          <ScrollView>
            <RadioButton.Group onValueChange={(value: any) => actions.setPlayers(value)} value={state.active.players}>
              <RadioButton.Item
                label="One"
                value="1"
              />
              <RadioButton.Item
                label="Two"
                value="2"
              />
              <RadioButton.Item
                label="Three"
                value="3"
              />
              <RadioButton.Item
                label="Four"
                value="4"
              />
            </RadioButton.Group>
          </ScrollView>
        </Modal>
      </Portal>
      <Button style={styles.button} mode="contained" onPress={showModal}>
        <View style={styles.iconWrapper}>
          <Text style={styles.icon}><Icon name="shield-account" size={50} /></Text>
          <Text style={styles.label} >Set Players</Text>
        </View>
      </Button>
    </React.Fragment>
  )
}


const styles = StyleSheet.create({
  button: {
    width: '50%',
    height: 150,
    display: 'flex',
    flexDirection: 'row',
    borderRadius: 0,
    alignContent: 'center',
    alignItems: 'center',
    justifyContent: 'center'
  },
  iconWrapper: {
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'center',
    alignContent: 'stretch',
    alignItems: 'center'
  },
  icon: {
    color: "white",
  },
  label: {
    width: '100%',
    color: 'white',
    textAlign: 'center',
    textTransform: 'uppercase',
    fontWeight: '500',
    marginTop: 15
  }
});