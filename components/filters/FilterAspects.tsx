import React, { useMemo } from 'react'
import useFilters from "../../context/filters/filters";
import { Modal, Portal, Button, Provider, Text, Checkbox } from "react-native-paper";
import { ScrollView, View, StyleSheet } from "react-native";
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import global from '../../styles/global';

export default function FilterAspects() {
  const { state, dispatch, actions } = useFilters();
  const [visible, setVisible] = React.useState(false);

  const showModal = () => setVisible(true);
  const hideModal = () => setVisible(false);
  const allAspects = state.all.aspects && useMemo(() => (state.all.aspects), [state.all.aspects]);
  const [checked, setChecked] = React.useState(true);
  const containerStyle = { backgroundColor: "white", padding: 20 };
  return (
    <React.Fragment>
      <Portal>
        <Modal visible={visible} onDismiss={hideModal} contentContainerStyle={containerStyle}>
          <ScrollView>
            <Checkbox.Item
              label="Toggle All"
              status={checked ? "checked" : "unchecked"}
              onPress={() => {
                if (!checked === true) {
                  dispatch({ type: 'RESET_ASPECTS' });
                } else {
                  dispatch({ type: 'CLEAR_ASPECTS' });
                }
                setChecked(!checked);
              }}
            />
            {allAspects &&
              allAspects.map((aspect: any) => (
                <Checkbox.Item
                  label={aspect.name}
                  key={aspect.name}
                  status={actions.isActive(aspect, "aspects") ? "checked" : "unchecked"}
                  onPress={() => {
                    actions.toggle("aspects", aspect);
                  }}
                />
              ))}
          </ScrollView>
        </Modal>
      </Portal>
      <Button style={styles.button} mode="contained" onPress={showModal}>
        <View style={styles.iconWrapper}>
          <Text style={styles.icon}><Icon name="progress-wrench" size={50} /></Text>
          <Text style={styles.label} >Filter Aspects</Text>
        </View>
      </Button>
    </React.Fragment>
  )
}

const styles = StyleSheet.create({
  button: {
    width: '50%',
    height: 150,
    display: 'flex',
    flexDirection: 'row',
    borderRadius: 0,
    alignContent: 'center',
    alignItems: 'center',
    justifyContent: 'center'
  },
  iconWrapper: {
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'center',
    alignContent: 'stretch',
    alignItems: 'center'
  },
  icon: {
    color: "white",
  },
  label: {
    width: '100%',
    color: 'white',
    textAlign: 'center',
    textTransform: 'uppercase',
    fontWeight: '500',
    marginTop: 15
  }
});