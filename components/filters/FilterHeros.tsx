import React, { useMemo } from 'react'
import useFilters from "../../context/filters/filters";
import { Modal, Portal, Button, Provider, Text, Checkbox, Image } from "react-native-paper";
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { ScrollView, View, StyleSheet } from "react-native";

export default function FilterHeros() {
  const { state, dispatch, actions } = useFilters();
  const [visible, setVisible] = React.useState(false);

  const showModal = () => setVisible(true);
  const hideModal = () => setVisible(false);
  const allHeros = state.all.heros && useMemo(() => (state.all.heros), [state.all.heros]);
  const [checked, setChecked] = React.useState(true);
  const containerStyle = { backgroundColor: "white", padding: 20 };
  return (
    <React.Fragment>
      <Portal>
        <Modal visible={visible} onDismiss={hideModal} contentContainerStyle={containerStyle}>
          <ScrollView>
            <Checkbox.Item
              label="Toggle All"
              status={checked ? "checked" : "unchecked"}
              onPress={() => {
                if (!checked === true) {
                  dispatch({ type: 'RESET_HEROS' });
                } else {
                  dispatch({ type: 'CLEAR_HEROS' });
                }
                setChecked(!checked);
              }}
            />
            {allHeros &&
              allHeros.map((hero: any) => (
                <Checkbox.Item
                  label={hero.name}
                  key={hero.name}
                  status={actions.isActive(hero, "heros") ? "checked" : "unchecked"}
                  onPress={() => {
                    actions.toggle("heros", hero);
                  }}
                />
              ))}
          </ScrollView>
        </Modal>
      </Portal>
      <Button style={styles.button} contentStyle={{ display: 'flex', flexDirection: 'column', flexWrap: 'wrap' }} mode="contained" onPress={showModal}>
        <View style={styles.iconWrapper}>
          <Text style={styles.icon}><Icon name="shield-account" size={50} /></Text>
          <Text style={styles.label} >Filter Heros</Text>
        </View>
      </Button>
    </React.Fragment>
  )
}

const styles = StyleSheet.create({
  button: {
    width: '50%',
    height: 150,
    display: 'flex',
    flexDirection: 'row',
    borderRadius: 0,
    alignContent: 'center',
    alignItems: 'center',
    justifyContent: 'center'
  },
  iconWrapper: {
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'center',
    alignContent: 'stretch',
    alignItems: 'center'
  },
  icon: {
    color: "white",
  },
  label: {
    width: '100%',
    color: 'white',
    textAlign: 'center',
    textTransform: 'uppercase',
    fontWeight: '500',
    marginTop: 15
  }
});