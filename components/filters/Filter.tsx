import React from 'react'
import { Checkbox } from "react-native-paper";
import { ScrollView } from "react-native";

export default function Filter(props: any) {
  const { checkedAction, checkedAllAction, objects } = props;
  const [checked, setChecked] = React.useState(true);
  return (
    <ScrollView>
      <Checkbox.Item
        label="Toggle All"
        status={checked ? "checked" : "unchecked"}
        onPress={() => {
          checkedAllAction(checked);
          setChecked(!checked);
        }}
      />
      {objects &&
        objects.map((object: any) => (
          <Checkbox.Item
            label={object.name}
            key={object.name}
            status={objects.object ? "checked" : "unchecked"}
            onPress={() => {
              checkedAction(object);
            }}
          />
        ))}
    </ScrollView>
  )
}
