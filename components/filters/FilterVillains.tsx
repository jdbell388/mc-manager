import React, { useMemo } from 'react'
import useFilters from "../../context/filters/filters";
import { Modal, Portal, Button, Provider, Text, Checkbox } from "react-native-paper";
import { ScrollView, View, StyleSheet } from "react-native";
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

export default function FilterVillains() {
  const { state, dispatch, actions } = useFilters();
  const [visible, setVisible] = React.useState(false);

  const showModal = () => setVisible(true);
  const hideModal = () => setVisible(false);
  const allVillains = state.all.villains && useMemo(() => (state.all.villains), [state.all.villains]);
  const [checked, setChecked] = React.useState(true);
  const containerStyle = { backgroundColor: "white", padding: 20 };
  return (
    <React.Fragment>
      <Portal>
        <Modal visible={visible} onDismiss={hideModal} contentContainerStyle={containerStyle}>
          <ScrollView>
            <Checkbox.Item
              label="Toggle All"
              status={checked ? "checked" : "unchecked"}
              onPress={() => {
                if (!checked === true) {
                  dispatch({ type: 'RESET_VILLAINS' });
                } else {
                  dispatch({ type: 'CLEAR_VILLAINS' });
                }
                setChecked(!checked);
              }}
            />
            {allVillains &&
              allVillains.map((villain: any) => (
                <Checkbox.Item
                  label={villain.name}
                  key={villain.name}
                  status={actions.isActive(villain, "villains") ? "checked" : "unchecked"}
                  onPress={() => {
                    actions.toggle("villains", villain);
                  }}
                />
              ))}
          </ScrollView>
        </Modal>
      </Portal>
      <Button style={styles.button} mode="contained" onPress={showModal}>
        <View style={styles.iconWrapper}>
          <Text style={styles.icon}><Icon name="emoticon-devil" size={50} /></Text>
          <Text style={styles.label} >Filter Villains</Text>
        </View>
      </Button>
    </React.Fragment>
  )
}

const styles = StyleSheet.create({
  button: {
    width: '50%',
    height: 150,
    display: 'flex',
    flexDirection: 'row',
    borderRadius: 0,
    alignContent: 'center',
    alignItems: 'center',
    justifyContent: 'center'
  },
  iconWrapper: {
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'center',
    alignContent: 'stretch',
    alignItems: 'center'
  },
  icon: {
    color: "white",
  },
  label: {
    width: '100%',
    color: 'white',
    textAlign: 'center',
    textTransform: 'uppercase',
    fontWeight: '500',
    marginTop: 15
  }
});